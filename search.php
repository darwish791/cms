<?php include "includes/db.php" ?>
<?php include "includes/header.php" ?>
    

            <!-- Navigation -->
            
<?php include "includes/navigation.php" ?>
           
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
               
                <?php  
                if(isset($_POST['submit'])) {
                    $search = $_POST['search'];
                    $query = "SELECT * FROM posts WHERE post_tags LIKE \"%$search%\"";
                    $select_all_post_tags_query = mysqli_query($connection, $query);
                    
                    if(!$select_all_post_tags_query) {
                        die("QUERY FAILED" . mysqli_error($connection));
                    }
                    
                    $count = mysqli_num_rows($select_all_post_tags_query);
                    
                    if($count == 0) {
                        echo "<h1>NO RESULT</h1>";
                    } else {
                        while($row = mysqli_fetch_assoc($select_all_post_tags_query)) {
                            $post_title = $row['post_title'];
                            $post_author = $row['post_author'];
                            $post_date = $row['post_date'];
                            $post_picture = $row['post_picture'];
                            $post_content = $row['post_content'];
                            $post_tags = $row['post_tags'];
                            $post_comment_count = $row['post_comment_count'];
                            $post_status = $row['post_status'];
                            // echo "<li><a href=\"#\">{$post_title}</a></li>";
                        ?>
                        
                        

                        <h1 class="page-header">
                            Page Heading
                            <small>Secondary Text</small>
                        </h1>

                        <!-- First Blog Post -->
                        <h2>
                            <a href="#"><?php echo $post_title; ?></a>
                        </h2>
                        <p class="lead">
                            by <a href="index.php"><?php echo $post_author; ?></a>
                        </p>
                        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
                        <hr>
                        <img class="img-responsive" src="images/<?php echo $post_picture; ?>" alt="">
                        <hr>
                        <p><?php echo $post_content; ?></p>
                        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                        <?php 
                        }
                    }
                } ?>
                

                
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>
        
<?php include "includes/footer.php" ?>