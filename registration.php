<?php  include "includes/db.php"; ?>
<?php  include "includes/header.php"; ?>

<?php 

if(isset($_POST['submit'])){

	$username = mysqli_real_escape_string($connection, $_POST['username']);
	$email = mysqli_real_escape_string($connection, $_POST['email']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);

	if(!empty($username) && !empty($email) && !empty($password)){

		$password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

		$query_register = "INSERT INTO users (username, user_email, user_password, user_role) ";
		$query_register .= "VALUES ('{$username}', '{$email}', '{$password}', 'subscriber' ) ";
		$result_register = mysqli_query($connection, $query_register);
		if(!$result_register){
			die("Failed: " . mysqli_errror($connection));
		}else{
			echo "OK";
		}

		$message = "Registration success";
	}else{
		$message = "Fields cannot be empty";
	}	
}else{
	$message = "";
}

 ?>

<!-- Navigation -->
<?php  include "includes/navigation.php"; ?>
    
<!-- Page Content -->
<div class="container">   
	<section id="login">
			<div class="container">
					<div class="row">
							<div class="col-xs-6 col-xs-offset-3">
									<div class="form-wrap">
									<h1>Register</h1>
											<form role="form" action="registration.php" method="post" id="login-form" autocomplete="off">
													<h6 class="text-center"><?php echo $message; ?></h6>
													<div class="form-group">
															<label for="username" class="sr-only">username</label>
															<input type="text" name="username" id="username" class="form-control" placeholder="Desired Username">
													</div>
													<div class="form-group">
															<label for="email" class="sr-only">Email</label>
															<input type="email" name="email" id="email" class="form-control" placeholder="lando_norris@example.com">
													</div>
													<div class="form-group">
															<label for="password" class="sr-only">Password</label>
															<input type="password" name="password" id="key" class="form-control" placeholder="MySeCuReDpasssword876">
													</div>
									
													<input type="submit" name="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Register">
											</form>
									
									</div>
							</div> <!-- /.col-xs-12 -->
					</div> <!-- /.row -->
			</div> <!-- /.container -->
	</section>
</div>


<hr>



<?php include "includes/footer.php";?>
