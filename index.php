<?php include "includes/db.php" ?>
<?php include "includes/header.php" ?>
    

  <!-- Navigation -->
            
<?php include "includes/navigation.php" ?>
           
  <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
  </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
							
							<?php

							if(isset($_GET['page'])){
								$page = $_GET['page'];
							}else{
								$page = 1;
							}

							if($page == "" || $page == 1){
								$page_1 = 0;
							}else{
								$page_1 = ($page * 5) - 5;
							}

							$query_post_count = "SELECT * FROM posts";
							$result_post_count = mysqli_query($connection, $query_post_count);			
							$count_post = mysqli_num_rows($result_post_count);

							$count_post = ceil($count_post / 5);

							$query = "SELECT * FROM posts LIMIT $page_1, 5";
							$select_all_post_query = mysqli_query($connection, $query);
							while($row = mysqli_fetch_assoc($select_all_post_query)) :
								$post_id = $row['post_id'];
								$post_title = $row['post_title'];
								$post_author = $row['post_author'];
								$post_date = $row['post_date'];
								$post_picture = $row['post_picture'];
								$post_content = substr($row['post_content'], 0, 100);
								$post_tags = $row['post_tags'];
								$post_comment_count = $row['post_comment_count'];
								$post_status = $row['post_status'];

								if($post_status == "published"):
								?>
									<h1 class="page-header">
											Page Heading
											<small>Secondary Text</small>
									</h1>

									<!-- First Blog Post -->
									<h2>
											<a href="post.php?p_id=<?php echo $post_id ?>"><?php echo $post_title; ?></a>
									</h2>
									<p class="lead">
											by <a href="index.php"><?php echo $post_author; ?></a>
									</p>
									<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
									<hr>
									<a href="post.php?p_id=<?php echo $post_id ?>">
										<img class="img-responsive" src="images/<?php echo $post_picture; ?>" alt="">
									</a>
									<hr>
									<p><?php echo $post_content; ?></p>
									<a class="btn btn-primary" href="post.php?p_id=<?php echo $post_id ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

									<?php endif ?>
							<?php endwhile ?>
            </div>
            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>
        </div>
        <!-- /.row -->
        <hr>

				<ul class="pager">
					<?php for($i = 1; $i <= $count_post; $i++): ?>
						<?php if($i == $page): ?>
							<li><a class="active_link" href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
						<?php else: ?>
							<li><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
						<?php endif ?>
					<?php endfor  ?>
				</ul>
        
<?php include "includes/footer.php" ?>  