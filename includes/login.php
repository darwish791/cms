<?php include "db.php" ?>
<?php session_start(); ?>

<?php 

if(isset($_POST['login'])){
	 
	$username = mysqli_real_escape_string($connection, $_POST['username']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);

	$query_login = "SELECT * FROM users WHERE username = '$username'";
	$result_login = mysqli_query($connection, $query_login);

	while($row_login = mysqli_fetch_array($result_login)){

		$db_user_id = $row_login['user_id'];
		$db_username = $row_login['username'];
		$db_user_password = $row_login['user_password'];
		$db_user_firstname = $row_login['user_firstname'];
		$db_user_lastname = $row_login['user_lastname'];
		$db_user_role = $row_login['user_role'];
	}

	if(password_verify($password, $db_user_password )){

		$_SESSION['username'] = $db_username;
		$_SESSION['firstname'] = $db_user_firstname;
		$_SESSION['lastname'] = $db_user_lastname;
		$_SESSION['user_role'] = $db_user_role;

		header("Location: ../admin/index.php");
	}else{
		header("Location: ../index.php");
	}
}

 ?>