<?php include "includes/admin_header.php"; ?>

<?php 

	if(isset($_SESSION['username'])){

		$username = $_SESSION['username'];

		$query = "SELECT * FROM users WHERE username = '$username'";
		$result = mysqli_query($connection, $query);
		while($row = mysqli_fetch_array($result)){

			$user_id = $row['user_id'];
			$username = $row['username'];	
			$user_password = $row['user_password'];	
			$user_firstname = $row['user_firstname'];
			$user_lastname = $row['user_lastname'];
			$user_email = $row['user_email'];
			$user_image = $row['user_image'];
			$user_role = $row['user_role'];
		}
	}
 ?>

 <?php 
 
 	if(isset($_POST['update_profile'])){ // when user click update button after edit form

	$user_firstname = $_POST['user_firstname'];
	$user_lastname = $_POST['user_lastname'];
	$username = $_POST['username'];
	$user_email = $_POST['user_email'];
	$user_password = $_POST['user_password'];
	// $post_date = date('d-m-y');

	$query_edit_user = "UPDATE users SET user_firstname = '$user_firstname', user_lastname = '$user_lastname', username = '$username', user_email = '$user_email', user_password = '$user_password' WHERE username = '$username'";
	$result_edit_user = mysqli_query($connection, $query_edit_user);
	header("Location: profile.php");
	}
 
 
  ?>


<div id="wrapper">

<!-- Navigation -->
<?php include "includes/admin_navigation.php"; ?>

	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Welcome Admin<small>Subheading</small></h1> 
					<form action="" method="post" enctype="multipart/form-data">

						<div class="form-group">
							<label for="firstname">First Name</label>
							<input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname ?>">
						</div>

						<div class="form-group">
							<label for="firstname">Last Name</label>
							<input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname ?>">
						</div>

						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" name="username" value="<?php echo $username ?>">
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="user_email" value="<?php echo $user_email ?>">
						</div>

						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="user_password" autocomplete="off">
						</div>

						<div class="form-group">
							<input type="submit" class="btn btn-primary" name="update_profile" value="Update Profile">
						</div>

					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
  <!-- /#page-wrapper -->

<?php include "includes/admin_footer.php"; ?>


