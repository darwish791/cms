<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Comment Author</th>
			<th>Comment Content</th>
			<th>Comment Email</th>
			<th>Comment Status</th>
			<th>In Response To</th>
			<th>Date</th>
			<th>Approved</th>
			<th>Unapproved</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>

	<?php 
	$query_display_comments = "SELECT * from comments";
	$result_display_comments = mysqli_query($connection, $query_display_comments);
	while($row_display_comments = mysqli_fetch_assoc($result_display_comments)){

		$comment_id = $row_display_comments['comment_id'];
		$comment_post_id = $row_display_comments['comment_post_id'];	
		$comment_author = $row_display_comments['comment_author'];
		$comment_content = $row_display_comments['comment_content'];
		$comment_email = $row_display_comments['comment_email'];
		$comment_status = $row_display_comments['comment_status'];
		$comment_date = $row_display_comments['comment_date'];

		echo "<tr>";
			echo "<td>$comment_id</td>";
			echo "<td>$comment_author</td>";
			echo "<td>$comment_content</td>";
			echo "<td>$comment_email</td>";
			echo "<td>$comment_status</td>";

			$query_post_author = "SELECT * from posts WHERE post_id = $comment_post_id";
			$result_post_author = mysqli_query($connection, $query_post_author);
			$row_post_author = mysqli_fetch_array($result_post_author);

			echo "<td><a href=\"../post.php?p_id=$comment_post_id\">{$row_post_author['post_title']}</a></td>";
			echo "<td>$comment_date</td>";
			echo "<td><a href=\"comments.php?approve=$comment_id\">Approve</a></td>";
			echo "<td><a href=\"comments.php?unapprove=$comment_id\">Unapprove</a></td>";                                  
			echo "<td><a href=\"comments.php?delete=$comment_id\">Delete</a></td>";                                    
		echo "</tr>";
	}
	?>
	
	</tbody>
</table> 

<?php 

if(isset($_GET['approve'])){
	
	$comment_id_to_approve = $_GET['approve'];

	$query_comment_id_to_approve = "UPDATE comments SET comment_status = \"APPROVED\" WHERE comment_id = $comment_id_to_approve";
	$_comment_id_to_approve = mysqli_query($connection, $query_comment_id_to_approve);
	header("Location: comments.php");
}

if(isset($_GET['unapprove'])){
	
	$comment_id_to_unapprove = $_GET['unapprove'];

	$query_comment_id_to_unapprove = "UPDATE comments SET comment_status = \"UNAPPROVED\" WHERE comment_id = $comment_id_to_unapprove";
	$_comment_id_to_unapprove = mysqli_query($connection, $query_comment_id_to_unapprove);
	header("Location: comments.php");
}

if(isset($_GET['delete'])){
	
	$comment_id_to_delete = $_GET['delete'];

	$query_comment_id_to_delete = "DELETE FROM comments WHERE comment_id = $comment_id_to_delete";
	$result = mysqli_query($connection, $query_comment_id_to_delete);
	header("Location: comments.php");
}
 ?>