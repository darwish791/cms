<?php 


	if(isset($_GET['edit'])){  // Get user id when user click edit

		$the_user_id = $_GET['edit'];  // user_id that need to be edit

		// run query to pull information to display information that need to display in form so user can edit

		$query_display_users = "SELECT * from users WHERE user_id = $the_user_id";
		$result_display_users = mysqli_query($connection, $query_display_users);
		while($row_display_users = mysqli_fetch_array($result_display_users)){

		$user_id_display_users = $row_display_users['user_id'];
		$username_display_users = $row_display_users['username'];	
		$user_password_display_users = $row_display_users['user_password'];	
		$user_firstname_display_users = $row_display_users['user_firstname'];
		$user_lastname_display_users = $row_display_users['user_lastname'];
		$user_email_display_users = $row_display_users['user_email'];
		$user_image_display_users = $row_display_users['user_image'];
		$user_role_display_users = $row_display_users['user_role'];
		}
	

		if(isset($_POST['edit_user'])){ // when user click update button after edit form

			$user_firstname = $_POST['user_firstname'];
			$user_lastname = $_POST['user_lastname'];	
			$user_role = $_POST['user_role'];
			$username = $_POST['username'];
			$user_email = $_POST['user_email'];
			$user_password = $_POST['user_password'];
			// $post_date = date('d-m-y');

			if(!empty($user_password)){
				$query_password = "SELECT * FROM user_password WHERE user_id = $the_user_id";
				$result_password = mysqli_query($connection, $query_password);
				$row_password = mysqli_fetch_array($result_password);

				$db_user_password = $row_password['user_password'];

				if($db_user_password != $user_password){
					$hashed_password = password_hash($db_user_password, PASSWORD_BCRYPT, array('cost' => 10));
				}
				
				$query_edit_user = "UPDATE users SET user_firstname = '$user_firstname', user_lastname = '$user_lastname', user_role = '$user_role', username = '$username', user_email = '$user_email', user_password = '$hashed_password' WHERE user_id = $the_user_id";
				$result_edit_user = mysqli_query($connection, $query_edit_user);
				header("Location: ./users.php");
				echo "User Updated" . "<a href='users.php'>View Users?</a>";
			}
		}

	}else{

		header("Location: index.php");
	}

 ?>


<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="firstname">First Name</label>
		<input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname_display_users ?>">
	</div>

	<div class="form-group">
		<label for="firstname">Last Name</label>
		<input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname_display_users ?>">
	</div>

	<div class="form-group">
		<label for="user_role">Role</label>
		<select name="user_role" id="" class="form-control">
			<option value="<?php echo $user_role_display_users ?>"><?php echo $user_role_display_users ?></option>
			<?php if($user_role_display_users == "admin"): ?>
				<option value="subscriber">Subscriber</option>
			<?php else: ?>
				<option value="admin">Admin</option>
			<?php endif ?>
		</select>
	</div>

	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" name="username" value="<?php echo $username_display_users ?>">
	</div>

	<div class="form-group">
		<label for="email">Email</label>
		<input type="email" class="form-control" name="user_email" value="<?php echo $user_email_display_users ?>">
	</div>

	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" name="user_password" autocomplete="off">
	</div>

	<!-- <div class="form-group">
		<label for="post_status">Post Status</label>	
		<select name="post_status" id="" class="form-control">
			<option value="published">Published</option>
			<option value="unpublished">Unpublished</option>
			<option value="draft">Draft</option>
		</select>
	</div> -->

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="edit_user" value="Update User">
	</div>

</form>