<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Firstname</th>
			<th>Lastname</th>
			<th>Email</th>
			<th>Role</th>
			<th style="text-align:center" colspan="2">Change role</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>

	<?php 
	$query_display_users = "SELECT * from users";
	$result_display_users = mysqli_query($connection, $query_display_users);
	while($row_display_users = mysqli_fetch_array($result_display_users)){

	$user_id = $row_display_users['user_id'];
	$username = $row_display_users['username'];	
	$user_password = $row_display_users['user_password'];	
	$user_firstname = $row_display_users['user_firstname'];
	$user_lastname = $row_display_users['user_lastname'];
	$user_email = $row_display_users['user_email'];
	$user_image = $row_display_users['user_image'];
	$user_role = $row_display_users['user_role'];
	// $comment_date = $result_display_users['comment_date'];

	echo "<tr>";
	echo "<td>$user_id</td>";
	echo "<td>$username</td>";
	echo "<td>$user_firstname</td>";
	echo "<td>$user_lastname</td>";
	echo "<td>$user_email</td>";
	echo "<td>$user_role</td>";
	echo "<td><a class=\"btn btn-primary\" href=\"./users.php?change_to_admin=$user_id\">Admin</a></td>";                                
	echo "<td><a class=\"btn btn-primary\" href=\"./users.php?change_to_subscriber=$user_id\">Subcriber</a></td>";                                
	echo "<td><a class=\"btn btn-success\" href=\"./users.php?source=edit_user&edit=$user_id\">Edit</a></td>";                                    
	echo "<td><a class=\"btn btn-danger\" href=\"./users.php?delete=$user_id\">Delete</a></td>";                                    
	echo "</tr>";
	}
	?>
	
	</tbody>
</table> 

<?php 

if(isset($_GET['change_to_admin'])){
	
	$user_id_to_make_admin = $_GET['change_to_admin'];

	$query_user_id_to_make_admin = "UPDATE users SET user_role = \"admin\" WHERE user_id = $user_id_to_make_admin";
	$result_user_id_to_make_admin = mysqli_query($connection, $query_user_id_to_make_admin);
	header("Location: users.php");
}

if(isset($_GET['change_to_subscriber'])){
	
	$user_id_to_make_subscriber = $_GET['change_to_subscriber'];

	$query_user_id_to_make_asubscriber = "UPDATE users SET user_role = \"subscriber\" WHERE user_id = $user_id_to_make_subscriber";
	$result_user_id_to_make_subscriber = mysqli_query($connection, $query_user_id_to_make_asubscriber);
	header("Location: users.php");
}

if(isset($_GET['delete'])){
	
	$user_id_to_delete = $_GET['delete'];

	$query_user_id_to_delete = "DELETE FROM users WHERE user_id = $user_id_to_delete";
	$result_user_id_to_delete = mysqli_query($connection, $query_user_id_to_delete);
	header("Location: users.php");
}
 ?>