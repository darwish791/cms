<?php 

	if(isset($_POST['create_user'])){

	$user_firstname = $_POST['user_firstname'];
	$user_lastname = $_POST['user_lastname'];	
	$user_role = $_POST['user_role'];
	$username = $_POST['username'];
	$user_email = $_POST['user_email'];
	$user_password = $_POST['user_password'];
	
	$user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 10));

	$query_create_user = "INSERT INTO users(user_firstname, user_lastname, user_role, username, user_email, user_password) ";
	$query_create_user .= "VALUES ('$user_firstname', '$user_lastname', '$user_role', '$username', '$user_email', '$user_password')";
	$result_create_user = mysqli_query($connection, $query_create_user);

	echo "User Created: " . " " . "<a href=\"users.php\" class=\"btn btn-primary\">View users</a>";
	}

 ?>


<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="firstname">First Name</label>
		<input type="text" class="form-control" name="user_firstname">
	</div>

	<div class="form-group">
		<label for="lastname">Last Name</label>
		<input type="text" class="form-control" name="user_lastname">
	</div>

	<div class="form-group">
		<select name="user_role" id="" class="form-control">
					<option value="subscriber">Select options</option>
					<option value="admin">Admin</option>
					<option value="subscriber">Subscriber</option>
		</select>
	</div>

	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" name="username">
	</div>

	<div class="form-group">
		<label for="email">Email</label>
		<input type="email" class="form-control" name="user_email">
	</div>

	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" name="user_password">
	</div>

	<!-- <div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
			<option value="published">Published</option>
			<option value="unpublished">Unpublished</option>
			<option value="draft">Draft</option>
		</select>
	</div> -->

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="create_user" value="Add User">
	</div>

</form>