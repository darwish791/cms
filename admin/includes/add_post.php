<?php 

	if(isset($_POST['create_post'])){

	$post_title = $_POST['title'];
	$post_category_id = $_POST['post_category_id'];
	$post_author = $_POST['post_author'];	
	$post_status = $_POST['post_status'];

	$post_image = $_FILES['post_image']['name'];
	$post_image_temp = $_FILES['post_image']['tmp_name'];

	$post_tags = $_POST['post_tags'];
	$post_content = $_POST['post_content'];
	$post_date = date('d-m-y');
	// $post_comment_count = 4;

	move_uploaded_file($post_image_temp, "../images/$post_image");

	$query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_picture, post_content, post_tags, post_status) ";
	$query .= "VALUES ($post_category_id, '$post_title', '$post_author', now(), '$post_image', '$post_content', '$post_tags', '$post_status')";
	$result = mysqli_query($connection, $query);


	$post_id = mysqli_insert_id($connection);
	echo "<p class=\"bg-success\">Post Created. <a position=\"right\" class=\"btn btn-primary\" href=\"../post.php?p_id=$post_id\">Go to the Post</a>  or  <a position=\"right\" class=\"btn btn-primary\" href=\"posts.php\">View all post</a></p>";

	}

 ?>


<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="title">Post Title</label>
		<input type="text" class="form-control" name="title">
	</div>

	<div class="form-group">
		<select name="post_category_id" id="" class="form-control">
				<?php $query_dropdown = "SELECT * FROM categories";
				$result_dropdown = mysqli_query($connection,$query_dropdown);
				while($row_dropdown = mysqli_fetch_assoc($result_dropdown)):	?>
					<option value="<?php echo $row_dropdown['cat_id']; ?>"><?php echo $row_dropdown['cat_title']; ?></option>
				<?php endwhile ?>
		</select>
	</div>

	<div class="form-group">
		<label for="author">Post Author</label>
		<input type="text" class="form-control" name="post_author">
	</div>

	<div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
			<option value="published">Published</option>
			<option value="unpublished">Unpublished</option>
			<option value="draft">Draft</option>
		</select>
	</div>

	<div class="form-group">
		<label for="post_image">Post Image</label>
		<input type="file" class="form-control" name="post_image">
	</div>

	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input type="text" class="form-control" name="post_tags">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<input type="text" class="form-control" name="post_content">
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="create_post" value="Publish Post">
	</div>

</form>