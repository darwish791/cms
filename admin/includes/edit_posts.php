<?php

	if(isset($_GET['p_id'])){
		
		$post_id = $post_id_to_display = ($_GET['p_id']);
		$query_display = "SELECT * FROM posts WHERE post_id=$post_id_to_display";
		$result_display = mysqli_query($connection,$query_display);
		$row_display = mysqli_fetch_array($result_display);
	}
?>

<?php 

if(isset($_POST['update_posts'])){

	$post_title = $_POST['title'];
	$post_category_id = $_POST['post_category_id'];
	$post_author = $_POST['post_author'];
	$post_status = $_POST['post_status'];

	$post_image = $_FILES['post_image']['name'];
	$post_image_temp = $_FILES['post_image']['tmp_name'];
	$post_tags = $_POST['post_tags'];
	$post_content = $_POST['post_content'];
	$post_date = date('d-m-y');
	$post_comment_count = 4;

	move_uploaded_file($post_image_temp, "../images/$post_image");

	if(empty($row_display['post_picture'])){
		$query_image = "SELECT * FROM posts WHERE post_id = $post_id";
		$result_image = mysqli_query($connection, $query_image);
		$row_image = mysqli_fetch_array($result_image);
		$row_display['post_picture'] = $row_image['post_picture'];
	}

	$query_edit = "UPDATE posts SET post_category_id = $post_category_id, post_title = '$post_title', post_author = '$post_author', post_date = now(), post_picture = '$post_image', post_content = '$post_content', post_tags = '$post_tags', post_comment_count = '$post_comment_count',post_status = '$post_status' WHERE post_id = $post_id";
	$result_edit = mysqli_query($connection, $query_edit);

	echo "<p class=\"bg-success\">Post Updated. <a position=\"right\" class=\"btn btn-primary\" href=\"../post.php?p_id=$post_id\">Go to Update Post</a>  or  <a position=\"right\" class=\"btn btn-primary\" href=\"posts.php\">Edit more post Post</a></p>";
}
?>

<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="title">Post Title</label>
		<input type="text" class="form-control" name="title" value="<?php echo $row_display['post_title'] ?>">
	</div>

	<div class="form-group">
		<select name="post_category_id" id="" class="form-control">
				<?php $query_dropdown_categories = "SELECT * FROM categories";
				$result_dropdown_categories = mysqli_query($connection,$query_dropdown_categories);
				while($row_dropdown_categories = mysqli_fetch_assoc($result_dropdown_categories)):	?>
					<option value="<?php echo $row_dropdown_categories['cat_id']; ?>"><?php echo $row_dropdown_categories['cat_title']; ?></option>
				<?php endwhile ?>
		</select>
	</div>

	<div class="form-group">
		<label for="author">Author</label>
		<input type="text" class="form-control" name="post_author" value="<?php echo $row_display['post_author'] ?>">
	</div>


	<div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
			<option value="Published" <?php echo ($row_display['post_status']== 'Published') ?'selected':'' ?> >Published</option>
			<option value="Draft" <?php echo ($row_display['post_status']== 'Draft') ?'selected':'' ?> >Draft</option>        
		</select>
	</div>


	<!-- <div class="form-group">
		<label for="post_status">Post Status</label>
		<input type="text" class="form-control" name="post_status" value="<?php echo $row_display['post_status'] ?>">
	</div> -->

	<div class="form-group">
		<label for="post_image">Post Image</label>
		<input type="file" class="form-control" name="post_image">
		<img src="../images/<?php echo $row_display['post_picture'] ?>" alt="" height="100px" height="100px">	
	</div>	

	<div class="form-group">
		<label for="post_tags">Post Tag</label>
		<input type="text" class="form-control" name="post_tags" value="<?php echo $row_display['post_tags'] ?>">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" name="post_content" cols="30" rows="50"><?php echo $row_display['post_content'] ?></textarea>
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="update_posts" value="Publish Post">
	</div>


</form>