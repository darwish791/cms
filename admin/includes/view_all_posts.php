<?php 

if(isset($_POST['checkBoxArray'])){

	foreach($_POST['checkBoxArray'] as $postValueId){

		 $bulk_options = $_POST['bulk_options'];

		 switch($bulk_options){
			case 'published':
				$query_published = "UPDATE posts SET post_status = '{$bulk_options}' WHERE post_id = {$postValueId}";
				$result_published = mysqli_query($connection, $query_published);
			 	break;
			case 'draft':
				$query_draft = "UPDATE posts SET post_status = '{$bulk_options}' WHERE post_id = {$postValueId}";
				$result_draft = mysqli_query($connection, $query_draft);
			 	break;
			case 'clone':
				$query_clone = "SELECT * FROM posts WHERE post_id = {$postValueId}";
				$result_clone = mysqli_query($connection, $query_clone);
				while($row_clone = mysqli_fetch_array($result_clone)){
					$post_title = $row_clone['post_title'];
					$post_category_id = $row_clone['post_category_id'];
					$post_author = $row_clone['post_author'];
					$post_author = $row_clone['post_author'];
					$post_status = $row_clone['post_status'];
					$post_picture = $row_clone['post_picture'];
					$post_content= $row_clone['post_content'];
					$post_tags= $row_clone['post_tags'];
					$post_comment_count= $row_clone['post_comment_count'];
				}

				$query_clone_2 = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_picture, post_content, post_tags, post_comment_count, post_status) ";
				$query_clone_2 .= "VALUES ($post_category_id, '$post_title', '$post_author', now(), '$post_picture', '$post_content', '$post_tags', $post_comment_count, '$post_status')";
				$result_clone_2 = mysqli_query($connection, $query_clone_2);
			 	break;
			case 'delete':
				$query_delete = "DELETE FROM posts WHERE post_id = {$postValueId}";
				$result_delete = mysqli_query($connection, $query_delete);
			 	break;
		 }
	}

	// echo "receiving data";
}

 ?>

<form action="" method="post">
	<table class="table table-bordered table-hover">

		<div id="bulkOptionsContainer" class="col-xs-4" style="padding: 0px">
			<select name="bulk_options" id="" class="form-control">
				<option value="">Select option</option>
				<option value="published">Publish</option>
				<option value="draft">Draft</option>
				<option value="clone">Clone</option>
				<option value="delete">Delete</option>
			</select>
		</div>

		<div class="col-xs-4">
			<input name="submit" type="submit" value="Apply" class="btn btn-success">
			<a href="posts.php?source=add_post" class="btn btn-primary">Add New Post</a>
		</div>

		<thead>
			<tr>
				<th><input id="selectAllBoxes" type="checkbox" name="" id=""></th>
				<th>ID</th>
				<th>Author</th>
				<th>Title</th>
				<th>Category</th>
				<th>Status</th>
				<th>Image</th>
				<th>Tags</th>
				<th>Comments</th>
				<th>Date</th>
				<th>Views Count</th>
				<th>View Post</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>

		<?php 
		$query = "SELECT * from posts ORDER BY post_id DESC";
		$select_all_posts = mysqli_query($connection, $query);
		while($row = mysqli_fetch_assoc($select_all_posts)){

		$post_id = $row['post_id'];
		$post_author = $row['post_author'];	
		$post_title = $row['post_title'];
		$post_category_id = $row['post_category_id'];
		$post_status = $row['post_status'];
		$post_picture = $row['post_picture'];
		$post_tags = $row['post_tags'];
		$post_comment_count = $row['post_comment_count'];
		$post_date = $row['post_date'];
		$post_views_count = $row['post_views_count'];

		echo "<tr>";
		echo "<td><input class=\"checkBoxes\" type=\"checkbox\" name=\"checkBoxArray[]\" value=\"$post_id\"></td>";
		echo "<td>$post_id</td>";
		echo "<td>$post_author</td>";
		echo "<td>$post_title</td>";

		$query_category_id = "SELECT * FROM categories WHERE cat_id = $post_category_id";
		$result_category_id = mysqli_query($connection, $query_category_id);
		$row_category_id = mysqli_fetch_array($result_category_id);

		echo "<td>{$row_category_id['cat_title']}</td>";
		echo "<td>$post_status</td>";
		echo "<td><img src=\"../images/$post_picture\" width=\"100px\"></img></td>";
		echo "<td>$post_tags</td>";

		$query_comment_count = "SELECT * FROM comments WHERE comment_post_id = $post_id";
		$result_comment_count = mysqli_query($connection, $query_comment_count);
		$row_comments_count = mysqli_fetch_array($result_comment_count);
		$comment_id = $row_comments_count['comment_id'];

		$count_comments = mysqli_num_rows($result_comment_count);

		echo "<td><a href=\"post_comments.php?id={$post_id}\">$count_comments</a></td>";
		echo "<td>$post_date</td>";
		echo "<td><a href=\"posts.php?reset=$post_id\">$post_views_count</a></td>";
		echo "<td><a class=\"btn btn-primary\" href=\"../post.php?p_id=$post_id\">View</a></td>";
		echo "<td><a class=\"btn btn-primary\" href=\"posts.php?source=edit_post&p_id=$post_id\">Edit</a></td>";
		echo "<td><a onCl ick=\"javascript: return confirm('Are you sure want to delete this'); \" class=\"btn btn-danger\" href=\"posts.php?delete=$post_id\">Delete</a></td>";                              
		echo "</tr>";
		}

		?>
		
		</tbody>
	</table> 
</form>

<?php 

if(isset($_GET['delete'])){
	
	$post_id_to_delete = $_GET['delete'];

	$query = "DELETE FROM posts WHERE post_id = $post_id_to_delete";
	$result = mysqli_query($connection, $query);
	header("Location: posts.php");
}

if(isset($_GET['reset'])){
	
	$post_id_to_reset = $_GET['reset'];

	$query_reset = "UPDATE posts SET post_views_count = 0 WHERE post_id =" . mysqli_real_escape_string($connection, $_GET['reset']) . " ";
	$result = mysqli_query($connection, $query_reset);
	header("Location: posts.php");
}
 ?>