<?php include "includes/admin_header.php"; ?>
    
        <div id="wrapper">

        <!-- Navigation -->
        <?php include "includes/admin_navigation.php"; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
											<h1 class="page-header">Welcome Admin <?php echo $_SESSION['username'] ?></h1>
                    </div>
                </div>
                <!-- /.row -->

                <!-- /.row -->        
								<div class="row">
										<div class="col-lg-3 col-md-6">
												<div class="panel panel-primary">
													<div class="panel-heading">
															<div class="row">
																<div class="col-xs-3">
																		<i class="fa fa-file-text fa-5x"></i>
																</div>
																<div class="col-xs-9 text-right">
																	<?php	
																	$query_number_of_post = "SELECT * FROM posts";
																	$result_number_of_post = mysqli_query($connection, $query_number_of_post);
																	$row_number_of_post = mysqli_num_rows($result_number_of_post);
																	 ?>	
																	<div class='huge'><?php echo $row_number_of_post ?></div>
																	<div>Posts</div>
																</div>
															</div>
														</div>
														<a href="posts.php">
															<div class="panel-footer">
																<span class="pull-left">View Details</span>
																<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																<div class="clearfix"></div>
															</div>
														</a>
												</div>
										</div>
										<div class="col-lg-3 col-md-6">
												<div class="panel panel-green">
														<div class="panel-heading">
																<div class="row">
																	<div class="col-xs-3">
																			<i class="fa fa-comments fa-5x"></i>
																	</div>
																	<div class="col-xs-9 text-right">
																		<?php	
						 												$query_number_of_comment = "SELECT * FROM comments";
																		$result_number_of_comment = mysqli_query($connection, $query_number_of_comment);
																		$row_number_of_comment = mysqli_num_rows($result_number_of_comment);
																		?>
																		<div class='huge'><?php echo $row_number_of_comment ?></div>
																		<div>Comments</div>
																	</div>
																</div>
														</div>
														<a href="comments.php">
																<div class="panel-footer">
																		<span class="pull-left">View Details</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																</div>
														</a>
												</div>
										</div>
										<div class="col-lg-3 col-md-6">
												<div class="panel panel-yellow">
														<div class="panel-heading">
																<div class="row">
																		<div class="col-xs-3">
																				<i class="fa fa-user fa-5x"></i>
																		</div>
																		<div class="col-xs-9 text-right">
																		<?php	
						 												$query_number_of_users = "SELECT * FROM users";
																		$result_number_of_users = mysqli_query($connection, $query_number_of_users);
																		$row_number_of_users = mysqli_num_rows($result_number_of_users);
																		?>
																		<div class='huge'><?php echo $row_number_of_users ?></div>
																				<div> Users</div>
																		</div>
																</div>
														</div>
														<a href="users.php">
																<div class="panel-footer">
																		<span class="pull-left">View Details</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																</div>
														</a>
												</div>
										</div>
										<div class="col-lg-3 col-md-6">
												<div class="panel panel-red">
														<div class="panel-heading">
																<div class="row">
																		<div class="col-xs-3">
																				<i class="fa fa-list fa-5x"></i>
																		</div>
																		<div class="col-xs-9 text-right">
																			<?php	
																			$query_number_of_categories = "SELECT * FROM categories";
																			$result_number_of_categories = mysqli_query($connection, $query_number_of_categories);
																			$row_number_of_categories = mysqli_num_rows($result_number_of_categories);
																			?>
																			<div class='huge'><?php echo $row_number_of_categories ?></div>
																			<div>Categories</div>
																		</div>
																</div>
														</div>
														<a href="categories.php">
																<div class="panel-footer">
																		<span class="pull-left">View Details</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																</div>
														</a>
												</div>
										</div>
								</div>
                <!-- /.row -->


								<?php 

								$query_number_of_post_published_count = "SELECT * FROM posts WHERE post_status = 'published'";
								$result_number_of_post_published_count = mysqli_query($connection, $query_number_of_post_published_count);
								$row_number_of_post_published_count = mysqli_num_rows($result_number_of_post_published_count);
								
								$query_number_of_post_draft_count = "SELECT * FROM posts WHERE post_status = 'draft'";
								$result_number_of_post_draft_count = mysqli_query($connection, $query_number_of_post_draft_count);
								$row_number_of_post_draft_count = mysqli_num_rows($result_number_of_post_draft_count);
								
								$query_number_of_comments_unapprove_count = "SELECT * FROM comments WHERE comment_status = 'UNAPPROVED'";
								$result_number_of_comments_unapprove_count = mysqli_query($connection, $query_number_of_comments_unapprove_count);
								$row_number_of_comments_unapprove_count = mysqli_num_rows($result_number_of_comments_unapprove_count);

								$query_number_of_user_role_count = "SELECT * FROM users WHERE user_role = 'subscriber'";
								$result_number_of_user_role_count = mysqli_query($connection, $query_number_of_user_role_count);
								$row_number_of_post_user_role_count = mysqli_num_rows($result_number_of_user_role_count);
								
								 ?>


								<div class="row">
									<script type="text/javascript">
										google.charts.load('current', {'packages':['bar']});
										google.charts.setOnLoadCallback(drawChart);

										function drawChart() {
											var data = google.visualization.arrayToDataTable([
												['Data', 'Count'],

												<?php
												
												$element_text = ['Active Posts', 'Draft Posts' , 'Published Posts' ,'Comments', 'Comments Unapprove', 'Users', 'Users Subscriber', 'Categories'];
												$element_count = [$row_number_of_post, $row_number_of_post_draft_count, $row_number_of_post_published_count, $row_number_of_comment, $row_number_of_comments_unapprove_count, $row_number_of_users, $row_number_of_post_user_role_count, $row_number_of_categories];

												for($i = 0; $i < 8; $i++){

													echo "['{$element_text[$i]}'" . "," . "{$element_count[$i]}],";
												}

												?>

												// ['Posts', 1000]
											]);

											var options = {
												chart: {
													title: '',
													subtitle: '',
												}
											};

											var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

											chart.draw(data, google.charts.Bar.convertOptions(options));
										}
									</script>
									<div id="columnchart_material" style="width: auto; height: 500px;"></div>
								</div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include "includes/admin_footer.php"; ?>