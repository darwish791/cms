<?php include "includes/admin_header.php"; ?>

<div id="wrapper">

<!-- Navigation -->
<?php include "includes/admin_navigation.php"; ?>

	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Welcome Admin<small>Subheading</small></h1> 

					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Comment Author</th>
								<th>Comment Content</th>
								<th>Comment Email</th>
								<th>Comment Status</th>
								<th>In Response To</th>
								<th>Date</th>
								<th>Approved</th>
								<th>Unapproved</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>

						<?php 
						$query = "SELECT * FROM comments WHERE comment_post_id =" . mysqli_real_escape_string($connection, $_GET['id']) . " ";
						$result = mysqli_query($connection, $query);
						while($row = mysqli_fetch_array($result)){

							$comment_id = $row['comment_id'];
							$comment_post_id = $row['comment_post_id'];	
							$comment_author = $row['comment_author'];
							$comment_content = $row['comment_content'];
							$comment_email = $row['comment_email'];
							$comment_status = $row['comment_status'];
							$comment_date = $row['comment_date'];

							echo "<tr>";
								echo "<td>$comment_id</td>";
								echo "<td>$comment_author</td>";
								echo "<td>$comment_content</td>";
								echo "<td>$comment_email</td>";
								echo "<td>$comment_status</td>";

								$query_post_author = "SELECT * from posts WHERE post_id = $comment_post_id";
								$result_post_author = mysqli_query($connection, $query_post_author);
								$row_post_author = mysqli_fetch_array($result_post_author);

								echo "<td><a href=\"../post.php?p_id=$comment_post_id\">{$row_post_author['post_title']}</a></td>";
								echo "<td>$comment_date</td>";
								echo "<td><a href=\"post_comments.php?approve=$comment_id\">Approve</a></td>";
								echo "<td><a href=\"post_comments.php?unapprove=$comment_id\">Unapprove</a></td>";                                  
								echo "<td><a href='post_comments.php?delete=$comment_id&id=" . $_GET['id'] . "'>Delete</a></td>";                                    
							echo "</tr>";
						}
						?>
						
						</tbody>
					</table> 

					<?php 

					if(isset($_GET['approve'])){
						
						$comment_id_to_approve = $_GET['approve'];

						$query_comment_id_to_approve = "UPDATE comments SET comment_status = \"APPROVED\" WHERE comment_id = $comment_id_to_approve";
						$_comment_id_to_approve = mysqli_query($connection, $query_comment_id_to_approve);
						header("Location: post_comments.php");
					}

					if(isset($_GET['unapprove'])){
						
						$comment_id_to_unapprove = $_GET['unapprove'];

						$query_comment_id_to_unapprove = "UPDATE comments SET comment_status = \"UNAPPROVED\" WHERE comment_id = $comment_id_to_unapprove";
						$_comment_id_to_unapprove = mysqli_query($connection, $query_comment_id_to_unapprove);
						header("Location: post_comments.php");
					}

					if(isset($_GET['delete'])){
						
						$comment_id_to_delete = $_GET['delete'];

						$query_comment_id_to_delete = "DELETE FROM comments WHERE comment_id = $comment_id_to_delete";
						$result = mysqli_query($connection, $query_comment_id_to_delete);
						header("Location: post_comments.php?id=" . $_GET['id'] . "");
					}
					?>

					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	<?php include "includes/admin_footer.php"; ?>