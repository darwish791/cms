<?php
	function insert_categories() {

        global $connection;

		if(isset($_POST['submit'])){
			$cat_title = $_POST['cat_title'];
			if($cat_title == "" || empty($cat_title)){
				echo "This field is required";
			}else{
				$query = "INSERT INTO categories(cat_title) VALUES ('$cat_title')";
				$result = mysqli_query($connection, $query);
				if(!$result){
					die("QUERY failed" . mysqli_error($connection));
				}
			}
		}
	}


    function findAllCategories() {

		global $connection;

        $query = "SELECT * from categories";
        $select_all_categories = mysqli_query($connection, $query);
        while($row = mysqli_fetch_assoc($select_all_categories)){

            $cat_id = $row['cat_id'];
            $cat_title = $row['cat_title'];

            echo "<tr>";
            echo "<td>{$cat_id}</td>";
            echo "<td>{$cat_title}</td>";
            echo "<td><a href=\"categories.php?delete={$cat_id}\">Delete</a></td>";                                    
            echo "<td><a href=\"categories.php?edit={$cat_id}\">Edit</a></td>";                                    
            echo "</tr>";
        }
    }

	function delete() {

		global $connection;
		
		if(isset($_GET['delete'])){

			$the_cat_id = $_GET['delete'];
			$query = "DELETE FROM categories WHERE cat_id = $the_cat_id";
			$result = mysqli_query($connection, $query);
			header("Location: categories.php");

			if(!$result){
				die("DELETE failed" . mysqli_error($connection));
			}
		}
	}

	function users_online(){

		global $connection;
		$session = session_id();
		$time = time();
		$time_out_in_seconds = 60;
		$time_out = $time - $time_out_in_seconds;

		$query = "SELECT * FROM users_online WHERE session = '$session'";
		$result = mysqli_query($connection, $query);
		$count = mysqli_num_rows($result);

		if($count == NULL){
			mysqli_query($connection, "INSERT INTO users_online(session, time) VALUES('$session', '$time')");
		}else{
			mysqli_query($connection, "UPDATE users_online SET time = '$time' WHERE session = '$session'");
		}
		$result_online_query = mysqli_query($connection, "SELECT * FROM users_online WHERE time > '$time_out'");
		return mysqli_num_rows($result_online_query);
	}
?>