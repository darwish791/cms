<?php include "includes/db.php" ?>
<?php include "includes/header.php" ?>
    

            <!-- Navigation -->
            
<?php include "includes/navigation.php" ?>
           
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                
                <?php

								if(isset($_GET['p_id'])):
									$post_id = $_GET['p_id'];

									$query_views_count = "UPDATE posts SET post_views_count = post_views_count + 1 WHERE post_id = $post_id";
									$result_views_count = mysqli_query($connection, $query_views_count);

                	$query = "SELECT * FROM posts WHERE post_id = $post_id";
									$select_all_post_query = mysqli_query($connection, $query);
									while($row = mysqli_fetch_assoc($select_all_post_query)):
										$post_id = $row['post_id'];
										$post_title = $row['post_title'];
										$post_author = $row['post_author'];
										$post_date = $row['post_date'];
										$post_picture = $row['post_picture'];
										$post_content = $row['post_content'];
										$post_tags = $row['post_tags'];
										$post_comment_count = $row['post_comment_count'];
										$post_status = $row['post_status'];
										// echo "<li><a href=\"#\">{$post_title}</a></li>";
										?>
										
										<h1 class="page-header">
												Page Heading
												<small>Secondary Text</small>
										</h1>

										<!-- First Blog Post -->
										<h2>
												<a href=""><?php echo $post_title; ?></a>
										</h2>
										<p class="lead">
												by <a href="author_posts.php?p_author=<?php echo $post_author; ?>&p_id=<?php echo $post_id; ?>"><?php echo $post_author; ?></a>
										</p>
										<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
										<hr>
										<img class="img-responsive" src="images/<?php echo $post_picture; ?>" alt="">
										<hr>
										<p><?php echo $post_content; ?></p>
									<?php endwhile ?>
                <?php else: ?>
									<?php header("Location: index.php"); ?>
                <?php endif ?>


								<!-- Blog Comments -->


								<?php 
								
									if(isset($_POST['create_comment'])){

										$post_id = $_GET['p_id'];

										$comment_author = $_POST['comment_author'];
										$comment_email = $_POST['comment_email'];
										$comment_content = $_POST['comment_content'];

										if(!empty($comment_author) && !empty($comment_email) && !empty($comment_content)){

											$query_create_comment = "INSERT INTO comments (comment_post_id, comment_author, comment_email, comment_content, comment_status, comment_date) ";
											$query_create_comment .= "VALUES ($post_id, '$comment_author', '$comment_email', '$comment_content', \"UNAPPROVE\", now())";
											$result_create_comment = mysqli_query($connection, $query_create_comment);


											// $query_post_count = "UPDATE posts SET post_comment_count = post_comment_count + 1 ";
											// $query_post_count .= "WHERE post_id = $post_id";
											// $result_post_count = mysqli_query($connection, $query_post_count);
										}else{
											echo "<script>alert('Fields cannot be empty')</script>";
										}
									}


									
								
								 ?>

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>

                    <form role="form" action="" method="post">
												<div class="form-group">
														<label for="author">Author</label>
                            <input type="text" class="form-control" name="comment_author">
                        </div>
												<div class="form-group">
														<label for="email">Email</label>
                            <input type="email" class="form-control" name="comment_email">
                        </div>
                        <div class="form-group">
														<label for="comment">Your comment</label>
                            <textarea class="form-control" rows="3" name="comment_content"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="create_comment">Submit</button>
                    </form>
                </div>

                <hr>




								<?php 
								
								$query_display_comments = "SELECT * FROM comments WHERE comment_post_id = $post_id ";
								$query_display_comments .= "AND comment_status = \"APPROVED\" ";
								$query_display_comments .= "ORDER BY comment_id DESC";
								$result_display_comments = mysqli_query($connection, $query_display_comments);

								while($row_display_comments = mysqli_fetch_array($result_display_comments)):
	
									$comment_date = $row_display_comments['comment_date'];
									$comment_content = $row_display_comments['comment_content'];
									$comment_author = $row_display_comments['comment_author'];
									

									?>

									<!-- Posted Comments -->

									<!-- Comment -->
									<div class="media">
											<a class="pull-left" href="#">
													<img class="media-object" src="http://placehold.it/64x64" alt="">
											</a>
											<div class="media-body">
													<h4 class="media-heading"><?php echo $comment_author ?>
															<small><?php echo $comment_date ?></small>
													</h4>
													<?php echo $comment_content ?>
											</div>
									</div>

								<?php endwhile ?>





                
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>
        
<?php include "includes/footer.php" ?>