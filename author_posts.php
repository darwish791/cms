<?php include "includes/db.php" ?>
<?php include "includes/header.php" ?>
    

            <!-- Navigation -->
            
<?php include "includes/navigation.php" ?>
           
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                
                <?php

								if(isset($_GET['p_id'])){
									$post_id = $_GET['p_id'];
									$post_author = $_GET['p_author'];
								}

                $query = "SELECT * FROM posts WHERE post_author = '$post_author'";
                $select_all_post_query = mysqli_query($connection, $query);
                while($row = mysqli_fetch_assoc($select_all_post_query)) {
                    $post_id = $row['post_id'];
                    $post_title = $row['post_title'];
                    $post_author = $row['post_author'];
                    $post_date = $row['post_date'];
                    $post_picture = $row['post_picture'];
                    $post_content = $row['post_content'];
                    $post_tags = $row['post_tags'];
                    $post_comment_count = $row['post_comment_count'];
                    $post_status = $row['post_status'];
                    // echo "<li><a href=\"#\">{$post_title}</a></li>";
                ?>
                
                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>

                <!-- First Blog Post -->
                <h2>
                    <a href=""><?php echo $post_title; ?></a>
                </h2>
                <p class="lead">
                    by <?php echo $post_author; ?>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
                <hr>
                <img class="img-responsive" src="images/<?php echo $post_picture; ?>" alt="">
                <hr>
                <p><?php echo $post_content; ?></p>
                <?php } ?>


								<!-- Blog Comments -->


								<?php 
								
									if(isset($_POST['create_comment'])){

										$post_id = $_GET['p_id'];

										$comment_author = $_POST['comment_author'];
										$comment_email = $_POST['comment_email'];
										$comment_content = $_POST['comment_content'];

										if(!empty($comment_author) && !empty($comment_email) && !empty($comment_content)){

											$query_create_comment = "INSERT INTO comments (comment_post_id, comment_author, comment_email, comment_content, comment_status, comment_date) ";
											$query_create_comment .= "VALUES ($post_id, '$comment_author', '$comment_email', '$comment_content', \"UNAPPROVE\", now())";
											$result_create_comment = mysqli_query($connection, $query_create_comment);


											$query_post_count = "UPDATE posts SET post_comment_count = post_comment_count + 1 ";
											$query_post_count .= "WHERE post_id = $post_id";
											$result_post_count = mysqli_query($connection, $query_post_count);
										}else{
											echo "<script>alert('Fields cannot be empty')</script>";
										}
									}


									
								
								 ?>  
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>
        
<?php include "includes/footer.php" ?>